CREATE DATABASE my_news_cover_II;

CREATE TABLE roles(
   id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
   name VARCHAR(20) NOT NULL
);

CREATE TABLE users(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  email VARCHAR(30) NOT NULL,
  first_name VARCHAR(30) NOT NULL,
  last_name VARCHAR(30) NOT NULL,
  password VARCHAR (30) NOT NULL,
  first_address VARCHAR(50) NOT NULL ,
  second_address VARCHAR (50) NOT NULL ,
  country VARCHAR (20) NOT NULL,
  city VARCHAR (20) NOT NULL ,
  zip VARCHAR (20) NOT NULL,
  phone VARCHAR (20) NOT NULL,
  role_id INT NOT NULL,
  CONSTRAINT FOREIGN KEY fk_roleId_rolesId (role_id) REFERENCES roles(id)
);

CREATE TABLE categories(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(30) NOT NULL
);

CREATE TABLE newssources(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  url VARCHAR(100) NOT NULL,
  name VARCHAR(30) NOT NULL,
  category_id INT NOT NULL,
  user_id INT NOT NULL,
  CONSTRAINT FOREIGN KEY fk_categoryId_categoriesId (category_id) REFERENCES categories(id),
  CONSTRAINT FOREIGN KEY fk_userID_usersId (user_id) REFERENCES users(id)
);

CREATE TABLE news(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  title VARCHAR(30) NOT NULL,
  short_description VARCHAR(100) NOT NULL,
  permanlink VARCHAR(100) NOT NULL,
  date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  news_source_id INT NOT NULL,
  user_id INT NOT NULL,
  category_id INT NOT NULL,
  CONSTRAINT FOREIGN KEY fk_newsSourceId_newsSourcesId (news_source_id) REFERENCES newssources(id),
  CONSTRAINT FOREIGN KEY fk_userIDI_usersId (user_id) REFERENCES users(id),
  CONSTRAINT FOREIGN KEY fk_categoryIdI_categoriesId (category_id) REFERENCES categories(id)
);



