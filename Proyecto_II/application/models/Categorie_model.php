
<?php

class Categorie_model extends CI_Model
{

    /**
     * Este metodo inserta una categoria en la base de datos
     */
    public function insert($categorie){
        $this->db->insert('categories',$categorie);
        if($this->db->affected_rows() >0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Este metodo carga todas las categorias de la base de datos
     */
    public function getAll(){
        $this->db->where('enable',true);
        $query = $this->db->get('categories');
        return $query->result_array();
    }

    /**
     * Este metodo desactiva una categoria de la base de datos
     */
    public function delete($id){
        $this->db->set('enable',false);
        $this->db->where('id',$id);
        $this->db->update('categories');
        if($this->db->affected_rows() > 0 ){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Este metodo obtiene una categoria por su id en la base de datos
     */
    public function getId($id){
        $this->db->where('id',$id);
        $query = $this->db->get('categories');
        return $query->row();
    }

    /**
     * Este metodo actualiza una categoria en la base de datos
     */
    public function update($id,$name){
        $this->db->set('name',$name);
        $this->db->where('id',$id);
        $this->db->update('categories');
        if($this->db->affected_rows() > 0 ){
            return true;
        }else{
            return false;
        }
    }
}