
<?php

class User_model extends CI_Model
{



    /**
     * Esta funcion inserta un usuario en la base de datos
     */
    public function insert($user)
    {
        $query = $this->db->insert('users', $user);
        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return 0;
        }
    }

    /**
     * Este metodo trae un registro de la base de datos
     */
    public function getId($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('users');
        return $query->row();
    }

    /**
     * Este metodo activa una cuenta con un correo en especifico
     */
    public function verification($email)
    {
        $this->db->where('email', $email);
        $query = $this->db->get('users');
        $result = $query->row();
        if ($result != null) { 
            $this->db->set('enable', true);
            $this->db->where('email', $email);
            $this->db->update('users');
        }
    }

    public function authentication($email, $password)
    {   
        $this->db->select('users.id,name');
        $this->db->from('users');
        $this->db->join('roles', 'users.role_id = roles.id');
        $this->db->where('email', $email);
        $this->db->where('password',$password);
        $this->db->where('enable',true);
        
        $query = $this->db->get();
        return $query->row();
    }

    /**
     * Este metodo hace publico un perfil
     */
    public function publicDashboard($user){
        $this->db->set('public',true);
        $this->db->where('id',$user->id);
        $this->db->update('users');
        if ($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }
    
}
