<?php

class News_model extends CI_Model
{

    /**
     * Este metodo elimina las noticias en la base de datos
     */
    public function delete(){
        $this->db->empty_table('news');
    }


    /**
     * Este metodo elimina las noticias de un usuario en especifico
     */
    public function deleteNewsFromUser($id){
        $this->db->where('user_id',$id);
        $this->db->delete('news');
    }

    /**
     * Este metodo inserta una noticia en la base de datos
     */
    public function insert($new,$row){
       
        $news_source_id = $row['id'];
        $user_id = $row['user_id'];
        $category_id = $row['category_id'];
        $title = str_replace("''","" ,$new->title);
        $short_description = str_replace("''","" , $new->description);
        $perman_link = $new->link;
        $date = $new->pubDate;
        $sql = "insert into news (title,short_description,permanlink,date,news_source_id,user_id,category_id)
        values('$title','$short_description','$perman_link','$date','$news_source_id','$user_id','$category_id')";
        
        $this->db->query($sql);
        return $this->db->insert_id();
    
    }

        /**
     * Este metodo inserta una noticia en la base de datos
     */
    public function insertNew($new,$row){
       
        $news_source_id = $row->id;
        $user_id = $row->user_id;
        $category_id = $row->category_id;
        $title = str_replace("''","" ,$new->title);
        $short_description = str_replace("''","" , $new->description);
        $perman_link = $new->link;
        $date = $new->pubDate;
        $sql = "insert into news (title,short_description,permanlink,date,news_source_id,user_id,category_id)
        values('$title','$short_description','$perman_link','$date','$news_source_id','$user_id','$category_id')";
      
        $this->db->query($sql);
        return $this->db->insert_id();
    
    }

    /**
     * Este metodo obtiene todas la noticias de la base de datos
     */
    public function getByUserId(){
        $user = $_SESSION['user'];
        $this->db->where('user_id',$user->id);
        $this->db->order_by('id','desc');
        $this->db->limit(200);
        $query = $this->db->get('news');
        return $query->result_array();
    }

    /**
     * Este metodo selecciona noticias por categoria
     */
    public function getByCategory($id){
        $this->db->where('category_id',$id);
        $query = $this->db->get('news');
        return $query->result_array();
    }

    /**
     * Este es el metodo busca titulos o contenido de una noticia
     */

    public function getByTitleOrBody($filter){
        $this->db->like('title',$filter);
        $this->db->or_like('short_description',$filter);
        $this->db->limit(200);
        $query = $this->db->get('news');
        return $query->result_array();
    }

    /**
     * Este metodo obtiien las noticas relacionadas a una etiqueta
     */
    public function getByTag($filter){
        $this->db->select('*');
        $this->db->from('news');
        $this->db->join('hashtag', 'news.id = hashtag.new_id','left');
        if(count($filter) > 0){
            $this->db->where('hashtag.name',$filter[0]);

        }
        if(count($filter) > 1){
            $this->db->or_where('hashtag.name',$filter[1]);
        }
        if(count($filter) > 2 ){
            $this->db->or_where('hashtag.name',$filter[2]);
        }
        $this->db->limit(200);
        $query = $this->db->get();
        return $query->result_array();
    }



}