
<?php

class NewsSources_model extends CI_Model
{


    /**
     * Este metodo devuelve las fuentes que tiene guardada un usuario en especifico
     */
    public function getSourcesFromUser($user){
        $this->db->select('newssources.id as newssources_id,url,newssources.name as newssources_name,category_id,user_id, categories.id as categories_id,categories.name as categories_name,categories.enable as categories_enable,newssources.enable as newssources_enable');
        $this->db->from('newssources');
        $this->db->join('categories', 'newssources.category_id = categories.id');
        $this->db->where('user_id',$user->id);
        $this->db->where('newssources.enable',true);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Este metodo me devuelve todas las fuentes de noticias en la base de datos
     */
    public function getAll(){
        $this->db->where('enable',true);
        $query = $this->db->get('newssources');
        return $query->result_array();
    }

    /**
     * Este metodo inserta una fuente de noticias en la base de datos
     */
    public function insert($source){
        $user = $_SESSION['user'];
        $source = [
        "user_id" => $user->id,
        "name" => $source['name'],
        "url" => $source['url'],
        "category_id" => $source['categorie']];
        $this->db->insert('newssources',$source);
        if($this->db->affected_rows() > 0){
            return $this->db->insert_id();
        }else{
            return 0;
        }

    }

    /**
     * Este metodo elimina un fuente de noticias
     */
    public function delete($id){
        $this->db->set('enable',false);
        $this->db->where('id',$id);
        $this->db->update('newssources');
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }

    }

    /**
     * Este metodo extrae la informacion de una fuente de noticias por su id
     */
    public function getId($id){
        $this->db->where('id',$id);
        $this->db->where('enable',true);
        $query = $this->db->get('newssources');
        return $query->row();
    }

    /**
     * Este metodo actualiza una fuente de noticias 
     */
    public function update($source){
        $this->db->set('name',$source['name']);
        $this->db->set('category_id',$source['categorie']);
        $this->db->set('url',$source['url']);
        $this->db->where('id',$source['id']);
        $this->db->update('newssources');
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }

    }
}