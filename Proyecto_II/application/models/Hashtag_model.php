<?php

class Hashtag_model extends CI_Model
{

    /**
     * Este metodo es el metodo utilizado para insertar un subcategoria en la tabla hashtag de la base datos, este metodo es usado solo por el metodo cronjob
     */
    public function insert($hashtag, $source, $newId)
    {

        $this->db->set('name', $hashtag);
        $this->db->set('new_id', $newId);
        $this->db->set('source_id', $source['id']);
        if(!$this->hashTagExist($hashtag)){   
            $this->db->set('duplicate',false);
        }else{
            $this->db->set('duplicate',true);
        }
        $this->db->insert('hashtag') or die();   
     
    }

     /**
     * Este metodo es el metodo utilizado para insertar un subcategoria en la tabla hashtag de la base datos, este metodo es usado solo cuando se inserta una nueva fuente de noticias
     */
    public function insertHastag($hashtag, $source, $newId){
        $this->db->set('name', $hashtag);
        $this->db->set('new_id', $newId);
        $this->db->set('source_id', $source->id);
        if(!$this->hashTagExist($hashtag)){   
            $this->db->set('duplicate',false);
        }else{
            $this->db->set('duplicate',true);
        }
        $this->db->insert('hashtag') or die();   
    }

    /**
     * Este metodo verifica si existe una subcategoria con el mismo nombre
     */
    public function hashTagExist($hashtag){
        $this->db->where('name',$hashtag);
        $query = $this->db->get('hashtag');
        if($query->row() != null){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Este metodo selecciona las subcategorias pertenecientes a un usuario
     */
    public function getByIdUser($id){
        $this->db->select('hashtag.id as hashtag_id,hashtag.name as hashtag_name');
        $this->db->from('hashtag');
        $this->db->join('news', 'hashtag.new_id = news.id');
        $this->db->where('news.user_id',$id);
        $this->db->where('hashtag.duplicate',false);
        $this->db->limit(200);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Este metodo elimina las subcategorias pertenecientes a una fuente de noticias
     */
    public function deleteFromSourceId($id){
        $this->db->where('source_id',$id);
        $this->db->delete('hashtag');
    }

    
}
