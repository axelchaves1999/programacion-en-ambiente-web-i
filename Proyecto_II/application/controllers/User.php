<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('NewsSources_model');
    }

    /**
     * Este metodo carga la vista de registro
     */
    public function add()
    {
        $this->load->view('user/register');
    }

    /**
     * Este funcion carga la vista de dashboard de las noticias
     */
    public function dashboard(){

    }

    /**
     * Este metodo carga la vista del login
     */
    public function login()
    {
        $user = isset($_SESSION['user']) ? $_SESSION['user'] : null;
        if($user != null){
            if($user->name != 'Usuario'){
                redirect('categories');
            }else{
                $this->validateNews();
            }
        }else{
            $this->load->view('user/login');  
        }
    }



    /**
     * Este metodo registra a un usuario en la base de datos
     */
    public function register()
    {
        $result = $this->User_model->insert($this->input->post());
        if($result != 0){
            $user = $this->User_model->getId($result);
            $this->email($user);
        }
    }


    /**
     * Este metodo envia un correo de verificacion al usuaro recien creado
     */
    public function email($user)
    {
        $to = $user->email;
        $subject = 'Verification';
        $message = 'Thanks for signing up!
        Your account has been created, you can login with the following credentials after you have activated your account by pressing the url below.
         
        ------------------------
        Name: '.$user->first_name.'
        Email: '.$user->email.'
        ------------------------
         
        Please click this link to activate your account:
        http://isw613.com/Isw613/Proyecto_II/index.php/user/verification?email='.$user->email.'';

        $result = mail($to, $subject, $message, 'From: axelchaves1999@gmail.com');
        redirect('login');
    }

    /**
     * Este metodo es el que activa una cuenta creada
     */
    public function verification(){
        $email = $this->input->get('email');
        if($email != null){
            $this->User_model->verification($email);
        }
        redirect('login');
    }

    /**
     * Este metodo es el encargado de validar el login
     */
    public function authentication(){
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $user = $this->User_model->authentication($email,$password);
        if($user != null){
            $_SESSION['user'] = $user;
            if($user->name != 'Usuario'){
                redirect('categories');

            }else{
                $this->validateNews();
            }
        }else{
            redirect('login');
        }
    }

    /**
     * Este metodo desloguea a un usuario
     */
    public function logOut(){
        unset($_SESSION['user']);
        unset($_SESSION['tags']);
        redirect('login');
    }


    /**
     * Este metodo valida si el usuario logueado cuenta con noticias
     */
    public function validateNews(){
        $user = $_SESSION['user'];
        if($user != null){
            $result = $this->NewsSources_model->getSourcesFromUser($user);
            if($result != null){
                redirect('dashboard');
            }else{
                redirect('newssources');
                
            }
        }
    }

    /**
     * Este metodo hace publico un perfil de usuario
     */
    public function publicDashboard(){
        $user = $_SESSION['user'];
        $result = $this->User_model->publicDashboard($user);
        if($result){
            redirect('newssources');
        }else{
            redirect('dashboard');
        }
    }

    
}
