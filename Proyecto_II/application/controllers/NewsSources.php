<?php
defined('BASEPATH') or exit('No direct script access allowed');

class NewsSources extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('News_model');
        $this->load->model('NewsSources_model');
        $this->load->model('Categorie_model');
        $this->load->model('User_model');
        $this->load->model('Hashtag_model');
    }


    public function newsSources()
    {
        $userLogued = $_SESSION['user'];

        if ($userLogued != null) {
            $user = $this->User_model->getId($userLogued->id);
            $sources = $this->NewsSources_model->getSourcesFromUser($user);
            $data['link'] = 'http://isw613.com/Isw613/Proyecto_II/index.php/users/'.$user->first_name.'/'.$user->last_name.'/'.$user->id.'';
            $data['sources'] = $sources;
            $this->load->view('NewsSources/newsSources', $data);
        }
    }

    /**
     * Este metodo carga las fuentes de noticias del usuario logueado
     */
    public function add()
    {
        $user = $_SESSION['user'];
        if ($user != null) {
            $categories = $this->Categorie_model->getAll();
            $data['categories'] = $categories;
            $this->load->view('NewsSources/newsSources_add', $data);
        }
    }

    /**
     * Este metodo guarda una fuente de noticias en la base de datos
     */
    public function insert()
    {
        $result = $this->NewsSources_model->insert($this->input->post());
        if ($result != 0) {

            $source = $this->NewsSources_model->getId($result);
            $this->insertNews($source);
            redirect('newssources');
        }
    }

    /**
     * Este funcion elimina una fuente de noticias
     */
    public function delete()
    {
        $id = $this->input->get('delete');
        $result = $this->NewsSources_model->delete($id);
        if ($result) {
            $user = $_SESSION['user'];
            $this->News_model->deleteNewsFromUser($user->id);
            $this->Hashtag_model->deleteFromSourceId($id);
            redirect('newssources');
        }
    }

    /**
     * Este metodo edita una fuente de noticias
     */
    public function edit()
    {
        $id = $this->input->get('edit');
        $categories = $this->Categorie_model->getAll();
        $data['categories'] = $categories;
        $data['source'] = $this->NewsSources_model->getId($id);
        $this->load->view('NewsSources/newsSources_edit', $data);
    }

    /**
     * Este metodo actualiza una fuente de noticias
     */
    public function update()
    {
        $result = $this->NewsSources_model->update($this->input->post());
        if ($result) {
            redirect('newssources');
        }
    }

    public function insertNews($source)
    {

        $ch = curl_init($source->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36');
        $feed = curl_exec($ch);
        $rss = new SimpleXMLElement($feed);
        foreach ($rss->channel->item as $item) {
            $result = $this->News_model->insertNew($item, $source);
            $list =  $item->category;

            foreach($list as $category){
                try {
                    $this->Hashtag_model->insertHastag($category,$source,$result); 
                    
                } catch (\Throwable $th) {
                    
                }
            }
        }
    }


   
}
