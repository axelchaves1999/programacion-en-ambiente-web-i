<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Categorie extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Categorie_model');
    }


        /**
     * Este metodo carga la vista de las categorias
     */
    public function categories(){
        $categories = $this->Categorie_model->getAll();
        $data['categories'] = $categories;
        $this->load->view('categorie/categories',$data);
    }

    /**
     * Este metodo carga la vista de el crud de categorias
     */
    public function categorieAdd(){
        $this->load->view('categorie/categorie_add');
      
    }


    /**
     * Este metodo inserta una categoria en la base de datos
     */
    public function insert(){
        $result = $this->Categorie_model->insert($this->input->post());
        if($result){
            redirect('categories');
        }else{
            redirect('categorieAdd');
        }
    }


    /**
     * Este metodo elimina una categoria de la base de datos
     */
    public function delete(){
        $id = $this->input->get('delete');
        $result = $this->Categorie_model->delete($id);
        if($result){
            redirect('categories');
        }else{
            redirect('categories');
        }
    }

    /**
     * Este metodo edita una categoria
     */
    public function edit(){
        $id = $this->input->get('edit');
        $data['categorie'] = $this->Categorie_model->getId($id);
        $this->load->view('categorie/categorie_edit',$data);
    }

    /**
     * Este metodo actualiza una categoria en la base de datos
     */
    public function update(){
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $result = $this->Categorie_model->update($id,$name);
        if($result){
            redirect('categories');
        }else{
            redirect('categorie/categorie_edit');
        }
    }


}