<?php
defined('BASEPATH') or exit('No direct script access allowed');

class News extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('News_model');
        $this->load->model('Categorie_model');
        $this->load->model('NewsSources_model');
        $this->load->model('Hashtag_model');
    }

    /**
     * Este metodo carga el dashboard con las noticias
     */
    public function dashboard()
    {
        $user = $_SESSION['user'];
        $news = $this->News_model->getByUserId($user);
        $categories = $this->Categorie_model->getAll();
        $hashtags = $this->Hashtag_model->getByIdUser($user->id);
        $data['hashtags'] = $hashtags;
        $data['news'] = $news;
        $data['categories'] = $categories;
        $this->load->view('News/dashboard', $data);
    }

    /**
     * Este metodo elimina las noticias en la base de datos
     */
    public function delete()
    {
        $this->News_model->delete();
    }

    /**
     * Este metodo es el encargado de ejecutar el script desde consola y guardar las noticias en la base de datos
     */
    public function cronjob()
    {
        $sources = $this->NewsSources_model->getAll();
        foreach($sources as $row) {

            $ch = curl_init($row['url']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36');


            $feed = curl_exec($ch);
            $rss = new SimpleXMLElement($feed);
            foreach ($rss->channel->item as $item) {
               
                $result = $this->News_model->insert($item,$row);
                $list =  $item->category;
                foreach($list as $category){
                    try {
                        $this->Hashtag_model->insert($category,$row,$result); 
                        
                    } catch (\Throwable $th) {
                        
                    }
                }
            }
        }
    }

    /**
     * Este metodo muestra la portada pulica de un usuario
     */
    public function ShowPublicDashboard($name,$lastName,$id){
        $news = $this->News_model->getByUserId($id);
        $data['news'] = $news;
        $this->load->view('News/publicDashboard', $data);
   
    }

    /**
     * Este metodo busca las noticias pertenecientes a una categoria en especifico
     */
    public function searchByCategory(){
        $user = $_SESSION['user'];
        $id = $this->input->get('id');
        $news = $this->News_model->getByCategory($id);
        $categories = $this->Categorie_model->getAll();
        $hashtags = $this->Hashtag_model->getByIdUser($user->id);
        $data['hashtags'] = $hashtags;
        $data['news'] = $news;
        $data['categories'] = $categories;
        $this->load->view('News/dashboard', $data);
    }

    /**
     * Este es el metodo busca titulos o contenido de una noticia
     */
    public function searchByTitleOrBody(){
        $user = $_SESSION['user'];
        $filter = $this->input->post('search');
        $categories = $this->Categorie_model->getAll();
        $hashtags = $this->Hashtag_model->getByIdUser($user->id);
        $news = $this->News_model->getByTitleOrBody($filter);
        $data['hashtags'] = $hashtags;
        $data['news'] = $news;
        $data['categories'] = $categories;
        $this->load->view('News/dashboard', $data);
    }

    /**
     * Este metodo busca noticias por categorias relacionadas
     */

    public function searchByTag(){
        $this->storageTag();
        $user = $_SESSION['user'];
        $filter = $_SESSION['tags'];
        $categories = $this->Categorie_model->getAll();
        $hashtags = $this->Hashtag_model->getByIdUser($user->id);
        $news = $this->News_model->getByTag($filter);
        $data['hashtags'] = $hashtags;
        $data['news'] = $news;
        $data['categories'] = $categories;
        $this->load->view('News/dashboard', $data);
        
    }

    /**
     * Este metodo guarda en una lista temporal las etiquetas para hacer busqueda
     */
    public function storageTag(){
        $tags = isset($_SESSION['tags']) ?$_SESSION['tags']: null;
        if($tags == null){
            $tags = array();
        }
        $hashtag = $this->input->get('name');
        if(!in_array($hashtag,$tags)){
            array_push($tags,$hashtag);
        }
        $_SESSION['tags'] = $tags;
        
    }

    /**
     * Este metodo es de prueba
     */
    public function prueba(){
        $user = $_SESSION['user'];
        $filter = $this->input->post('search');
        $news = $this->News_model->getByTitleOrBody($filter);
        echo json_encode($news);
    }
   
  
    
}
