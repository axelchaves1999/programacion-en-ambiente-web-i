<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

 

    <body class="">
        <div class="container">
            <form class="form-signin mt-5" method="post" action="<?php echo site_url('user/authentication')?>">
                <h1 class="h3 mb-3 font-weight-normal text-center">My news cover</h1>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Email address</label>
                    <input name='email' type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Password</label>
                    <input name = 'password' type="password" class="form-control" id="exampleInputPassword1">
                </div>
                <div class="mb-3">
                <small class="font-weight-bold">Don't have an account? <a class="text-danger " href="<?php echo site_url('UserAdd') ?>">Register</a></small>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>
        </div>
    </body>
</body>
</html>