<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Register</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>


  <div class="container mt-5">
    <form action="<?php echo site_url('user/register'); ?>" method="post">
      <div class="row">
        <div class="form-group col-md-6 col-sm-12">
          <label for="inputEmail4">First name</label>
          <input type="text" class="form-control" name="first_name" placeholder="First name">
        </div>
        <div class="form-group col-md-6 col-sm-12"">
        <label for=" inputPassword4">Last name</label>
          <input type="text" class="form-control" name="last_name" placeholder="Last name">
        </div>
      </div>

      <div class="row">
        <div class="form-group col-md-6 col-sm-12">
          <label for="inputEmail4">Email Address</label>
          <input type="email" class="form-control" name="email" placeholder="Email">
        </div>
        <div class="form-group col-md-6 col-sm-12"">
        <label for=" inputPassword4">Password</label>
          <input type="password" class="form-control" name="password" placeholder="Password">
        </div>
      </div>
      <div class="form-group">
        <label for="inputAddress">Address</label>
        <input type="text" class="form-control" name="first_address" placeholder="Address">
      </div>
      <div class="form-group">
        <label for="inputAddress2">Address 2</label>
        <input type="text" class="form-control" name="second_address" placeholder="Address 2">
      </div>
      <div class="row">
        <div class="form-group col-md-6 col-sm-12">
          <label for="inputEmail4">Country</label>
          <select class="form-control" name="country">
            <option value="" selected disabled hidden>Choose here</option>
            <option value="1">Costa Rica</option>
            <option value="2">USA</option>
            <option value="3">Brazil</option>
            <option value="4">Mexico</option>
            <option value="5">Canada</option>
          </select>
        </div>
        <div class="form-group col-md-6 col-sm-12"">
        <label for=" inputPassword4">City</label>
          <input type="text" class="form-control" name="city" placeholder="City">
        </div>
      </div>
      <div class="row">
        <div class="form-group col-md-6 col-sm-12">
          <label for="inputEmail4">Zip/Postal Code</label>
          <input type="text" class="form-control" name="zip" placeholder="Zip/Postal Code">
        </div>
        <div class="form-group col-md-6 col-sm-12"">
        <label for=" inputPassword4">Phone Number</label>
          <input type="text" class="form-control" name="phone" placeholder="Phone Number">
        </div>
      </div>
      <div class="row">
        <div class="form-group col-md-6 col-sm-12 mt-3">
          <button  type="submit" class="btn btn-primary ">Sign up</button>
          <a class="btn btn-secondary" href="<?php echo site_url('login') ?>">Cancel</a>
        </div>
      </div>
    </form>
  </div>

</body>

</html>