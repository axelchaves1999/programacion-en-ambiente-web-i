<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Noticias</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>

    

    <div class="container mt-5">
        <div class="jumbotron jumbotron-fluid">
            <div class="container text-center">
                <h1 class="display-5">Your unique News Cover</h1>
                <hr>
            </div>
        </div>
    </div>

  

    <div class="container mt-3">
        <div class="row justify-content-center ">
            <?php 
            foreach($news as $new): ?>

            <div class="card m-3" style="width: 15rem;">
                <div class="card-body">
                    <p class="card-text"><strong><?php echo $new['title'] ?></strong></p>
                    <p class="card-text"><?php echo $new['short_description'] ?></p>
                    <a href="#" class="card-link"><?php echo $new['permanlink'] ?></a>
                </div>
            </div>
            
            <?php endforeach ?>
        </div>
    </div>

</body>

</html>