<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Noticias</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body>

    <div class="container">
        <ul class="nav justify-content-end">
            <li class="nav-item">
                <a class="nav-link " href="<?php echo site_url('newssources') ?>">Mis fuentes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?php echo site_url('user/logOut') ?>">Cerrar session</a>
            </li>

        </ul>
    </div>

    <div class="container mt-5">
        <div class="jumbotron jumbotron-fluid">
            <div class="container text-center">
                <h1 class="display-5">Your unique News Cover</h1>
                <hr>
            </div>
        </div>
    </div>

    <div class="container ">
        <h3 class="text-center">Categories</h3>
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('dashboard') ?>">All news</a>
            </li>

            <?php
            foreach ($categories as $categorie) : ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo site_url('News/searchByCategory?id=' . $categorie['id']) ?>"><?php echo $categorie['name'] ?></a>
                </li>
            <?php endforeach ?>



        </ul>
    </div>

    <div class="container ">
        <h3 class="text-center">Etiquetas</h3>
        <ul class="nav">
            <?php
            foreach ($hashtags as $tag) : ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo site_url('News/searchByTag?name=' . $tag['hashtag_name']) ?>"><?php echo $tag['hashtag_name'] ?></a>
                </li>
            <?php endforeach ?>



        </ul>
    </div>

    <div class="container">
        <form action="<?php echo site_url('News/searchByTitleOrBody'); ?>" method="post">
            <div class="row justify-content-center">
                <div class="form-group col-md-6 col-sm-12 mt-3">
                    <input id="search" type="text" class="form-control" name="search" placeholder="Buscar noticia">

                </div>
            </div>
            <div class="row justify-content-center">
                <button class="btn btn-lg btn-primary " type="submit">Buscar</button>
            </div>
        </form>
    </div>


    <div class="container mt-3">
        <div id="container" class="row justify-content-center ">
            <?php
            foreach ($news as $new) : ?>

                <div class="card m-3" style="width: 15rem;">
                    <div class="card-body">
                        <p class="card-text"><strong><?php echo $new['title'] ?></strong></p>
                        <p class="card-text"><?php echo $new['short_description'] ?></p>
                        <a href="<?php echo $new['permanlink'] ?>" class="card-link" target="_blank"><?php echo $new['permanlink'] ?></a>
                        <p class="card-text"><?php echo substr($new['date'], 0, -6) ?></p>
                    </div>
                </div>

            <?php endforeach ?>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            let text = document.getElementById('search');
            let container = document.getElementById('container');
            text.addEventListener('keyup', () => {
                if (text.value.length > 2) {
                    $.ajax({
                        url: '<?php echo site_url('News/prueba') ?>',
                        type: 'post',
                        data: {
                            search: text.value
                        },
                        success: function(response) {
                            let datos = eval(response);
                            container.innerHTML = "";
                            for(let i of datos){
                                container.innerHTML += 
                                `<div class="card m-3" style="width: 15rem;">
                                    <div class="card-body">
                                        <p class="card-text"> <strong>${i.title}</strong></p>
                                         <p class="card-text"> ${i.short_description}</p>
                                        <a href="" class="card-link" target="_blank"> ${i.permanlink}</a>
                                    <p class="card-text"></p>
                                    </div>
                                 </div>`
                            };

                        },
                        error: function() {
                            console.log('error');
                        }

                    });

                }

            })

        })
    </script>

</body>

</html>