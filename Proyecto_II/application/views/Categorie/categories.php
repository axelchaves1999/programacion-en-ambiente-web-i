<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Categorias</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>

  <div class="container">
    <ul class="nav justify-content-end">
      <li class="nav-item">
        <a class="nav-link " href="<?php echo site_url('user/logOut') ?>">Cerrar session</a>
      </li>
    </ul>
  </div>

  <div class="container mt-5">
    <div class="jumbotron jumbotron-fluid">
      <div class="container">
        <h1 class="display-5 text-center">Categorias</h1>
      </div>
    </div>
  </div>

  <div class="container pb-3">
    <a class="btn btn-primary" href="<?php echo site_url('categorieAdd') ?>" role="button">Agregar Categoria</a>
  </div>


  <div class="container">
    <table class="table">
      <thead class="thead-dark">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nombre</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <?php
        $cont = 0;
        foreach ($categories as $categorie) : $cont++; ?>
          <tr>
            <td><?php echo $cont . '.' ?></td>
            <td><?php echo $categorie['name'] ?></td>
            <td>
              <a class="btn btn-success" href="<?php echo site_url('categorie/edit?edit=' . $categorie['id']) ?>" role="button">Editar</a>
              <a class="btn btn-danger" href="<?php echo site_url('categorie/delete?delete=' . $categorie['id']) ?>" role="button">Eliminar</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>

  </div>

</body>

</html>