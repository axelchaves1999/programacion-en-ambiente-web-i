<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Categorias</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>

    <div class="container mt-5">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-5 text-center">Categorias</h1>
            </div>
        </div>
    </div>

   

    <div class="container">
        <form method="post" action="<?php echo site_url('categorie/insert') ?>">
            <div class="form-group">
                <label for="exampleInputEmail1">Nombre de la categoria</label>


                <input name="name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="">
                <button type="submit" class="btn btn-primary mt-3">Registrar</button>
                <a class="btn btn-secondary mt-3" href="<?php echo site_url('categories') ?>">Cancel</a>

            </div>
        </form>
    </div>




</body>

</html>