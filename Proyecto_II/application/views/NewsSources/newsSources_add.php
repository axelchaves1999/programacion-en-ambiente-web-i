<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Categorias</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>


    <div class="container mt-3">
        <div class="jumbotron jumbotron-fluid">
            <div class="container text-center">
                <h1 class="display-4">New sources</h1>
                <hr>
            </div>
        </div>
    </div>

    <div class="container">
        <form method="post" action="<?php echo site_url('NewsSources/insert') ?>">

            <div class="form-group">
                <label for="exampleInputEmail1">Nombre de la fuente</label>
                <input name="name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="">
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">URl de la fuente</label>
                <input name="url" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="">
            </div>

            <div class="form-group">
                <label for="exampleFormControlSelect1">Categoria de la fuente</label>
                <select name="categorie" class="form-control" id="exampleFormControlSelect1">
                    <?php
                        foreach($categories as $categorie): ?>
                           <option value ="<?php echo $categorie['id'] ?>"><?php echo $categorie['name'] ?></option>
                        
                    <?php endforeach ?>
                </select>
            </div>

            <button type="submit" class="btn btn-primary mb-3">Registrar</button>
            <a class="btn btn-secondary mb-3" href="<?php echo site_url('newssources') ?>">Cancel</a>
         
        </form>
    </div>

</body>

</html>