<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Fuentes de noticias</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>

  <div class="container">
    <ul class="nav justify-content-end">
      <li class="nav-item">
        <a class="nav-link " href="<?php echo site_url('dashboard') ?>">Mi portada</a>
      </li>

      <li class="nav-item">
        <a class="nav-link " href="<?php echo site_url('user/logOut') ?>">Cerrar session</a>
      </li>


      <li class="mt-1">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
          Publicar portada
        </button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Comparte tu portada</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <?php
                  echo $link
                  ?>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-primary" href="<?php echo site_url('User/publicDashboard') ?>" role="button">Compartir</a>
              </div>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>


  <div class="container mt-5">
    <div class="jumbotron jumbotron-fluid">
      <div class="container">
        <h1 class="display-5 text-center">Fuentes de noticias</h1>
      </div>
    </div>
  </div>

  <div class="container pb-3">
    <a class="btn btn-primary" href="<?php echo site_url('newssourcesadd') ?>" role="button">Agregar fuente de noticias</a>
  </div>


  <div class="container">
    <table class="table">
      <thead class="thead-dark">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nombre</th>
          <th scope="col">URL</th>
          <th scope="col">Categoria</th>

          <th></th>
        </tr>
      </thead>
      <tbody>

        <?php
        $cont = 0;
        foreach ($sources as $source) : $cont++; ?>
          <tr>
            <td><?php echo $cont . '.' ?></td>
            <td><?php echo $source['newssources_name'] ?></td>
            <td><?php echo $source['url'] ?></td>
            <td><?php echo $source['categories_name'] ?></td>
            <td>
              <a class="btn btn-success" href="<?php echo site_url('newssourcesedit?edit=' . $source['newssources_id']) ?>" role="button">Editar</a>
              <a class="btn btn-danger" href="<?php echo site_url('newssourcesdelete?delete=' . $source['newssources_id']) ?>" role="button">Eliminar</a>
            </td>
          <?php endforeach ?>

      </tbody>
    </table>

  </div>

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>