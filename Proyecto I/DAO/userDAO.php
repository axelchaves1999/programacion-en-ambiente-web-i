<?php

require_once('connection.php');

if(isset($_POST['add'])){
   userInsert($_REQUEST);
}

if(isset($_POST['login'])){
    authenticate($_REQUEST);
}


/*
Esta funcion inserta un usuario en la base de datos, devuelve el usuario registrado en la session iniciada
 */
function userInsert($data){
    $con = openCon();
    $firstNamae = mysqli_real_escape_string($con,$data['firstName']);
    $lastName =  mysqli_real_escape_string($con,$data['lastName']);
    $email = mysqli_real_escape_string($con,$data['email']);
    $password = mysqli_real_escape_string($con,$data['password']);
    $address = mysqli_real_escape_string($con,$data['address']);
    $address2 = mysqli_real_escape_string($con,$data['address2']);
    $country =  mysqli_real_escape_string($con,$data['country']);
    $city = mysqli_real_escape_string($con,$data['city']);
    $zip = mysqli_real_escape_string($con,$data['zip']);
    $phone =  mysqli_real_escape_string($con,$data['phone']);
    $sql = "insert into users(email,first_name,last_name,password,first_address,second_address,country,city,zip,
    phone,role_id) values('$email','$firstNamae','$lastName','$password','$address','$address2','$country','$city','$zip','$phone','2')";

    $result = mysqli_query($con,$sql) or die('error'.$con-> error);
    
    if($result){
        $lastId = mysqli_insert_id($con);
        session_start();
        $_SESSION['user'] = userSelectId($lastId);
        header('location: ../index.php');
        closeCon($con);
    }else{
        closeCon($con);
        header('location: ../index.php?register=true');
    }
  
    
    
}


/*
    Esta funcion selecciona un usuario de la base de datos por su id que es pasado por parametro
*/
function userSelectId($id){
    $con = openCon();
    $sql = "select id,name,surname,email,password,type from users where id='$id'";
    $result = mysqli_query($con,$sql);
    return mysqli_fetch_array($result);
}


/*
Esta funcion verifica si el usuario existe en la base de datos lo selecciona y trae algunos datos, los envia por medio del session
*/
function  authenticate($data){
    $con = openCon();
    $email = mysqli_real_escape_string($con,$data['email']);
    $password = mysqli_real_escape_string($con,$data['password']);
    $sql = "select users.id as user_id,first_name,last_name,email,roles.name as role_name from users join roles on role_id = roles.id where email = '$email' and password = '$password'";
    $result = mysqli_query($con,$sql) or die($con->error);
    if($result->num_rows == 1){
        session_start();
        $_SESSION['user'] = mysqli_fetch_array($result);
    }
    header('location: ../index.php');
}