<?php
include('connection.php');

$id = 0;
$name = "";

if(isset($_POST['add'])){
    insert($_REQUEST);
}

if(isset($_GET['delete'])){
    delete($_REQUEST);
}

if(isset($_GET['edit'])){
    edit($_REQUEST);
}

if(isset($_POST['update'])){
    update($_REQUEST);
}

/**
 * Este metodo inserta una cateogria en la base de datos
 */
function insert($data){
    $con = openCon();
    $name = mysqli_real_escape_string($con,$data['name']);
    $sql = "insert into categories (name) values('$name')";
    if(mysqli_query($con,$sql)){

    }
    closeCon($con);
    header('Location: ../index.php');

}

/**
 * Este metodo desabilita una categoria en la base de datos
 */
function delete($data){
    $con = openCon();
    $id = mysqli_real_escape_string($con,$data['delete']);
    $sql = "update categories set disable = false where id='$id'";
    if(mysqli_query($con,$sql)){

    }
    closeCon($con);
    header('location: ../index.php');
}

/**
 * Este metodo selecciona el id,name de una categoria de la base de datos
 */
function edit($data){
    $con = openCon();
    $id = mysqli_real_escape_string($con,$data['edit']);
    $sql = "select id,name from categories where id ='$id'";
    $result = mysqli_query($con,$sql);
    if($result->num_rows ==1){
        $row = $result->fetch_array();
        $GLOBALS['id'] = $row['id'];
        $GLOBALS['name'] = $row['name'];
    }
    closeCon($con);
}

/**
 * Este metodo selecciona todas la categorias de la base de datos que no esten desabilitadas
 */
function select(){
    $con = openCon();
    $sql = "select id,name from categories where disable = true";
    return $result = mysqli_query($con,$sql);
}

/**
 * Este metodo actualiza una categoria con nuevos datos en la base de datos
 */
function update($data){
    $con =openCon();
    $id = mysqli_real_escape_string($con,$data['id']);
    $name = mysqli_real_escape_string($con,$data['name']);
    $sql = "update categories set name= '$name' where id = '$id'";
    if(mysqli_query($con,$sql)){

    }
    header('location: ../index.php');
}