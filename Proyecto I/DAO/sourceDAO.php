<?php

require_once('connection.php');

$sourceId = 0;
$name = '';
$url = '';
$category = 0;

if(isset($_POST['add'])){
    insertSource($_REQUEST);
}

if(isset($_GET['delete'])){
    deleteSource($_REQUEST);
}

if(isset($_GET['edit'])){
    editSource($_REQUEST);
}

if(isset($_POST['update'])){
    updateSource($_REQUEST);
}

/**
 * Este metodo inserta una fuente de noticias en la base de datos
 */
function insertSource($data){
    $con = openCon();
    session_start();
    $user = $_SESSION['user'];
    $id = $user['user_id'];
    $name = mysqli_real_escape_string($con,$data['name']);
    $url = mysqli_real_escape_string($con,$data['url']);
    $category = mysqli_real_escape_string($con,$data['select']);
    $sql = "insert into newssources (url,name,category_id,user_id) values('$url','$name','$category','$id')";
    mysqli_query($con,$sql) or die($con->error);
        
    closeCon($con);
    insertNews();
    header("location: ../index.php?register");
}


/**
 * Este metodo inserta una notica en la tabla news de la base de datos
 */
function insertNews(){
    $sources = getFeed();
    while($row = mysqli_fetch_array($sources)){

        $ch = curl_init($row['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36');
        
        
        $feed = curl_exec($ch);
        $rss = new SimpleXMLElement($feed);
        foreach($rss->channel->item as $item) {
            insertNewFromUser($item,$row);
        }
    }

}


/**
 * Este metodo inserta una notica en la tabla news de la base de datos
 */
function insertNewFromUser($new,$row){
    $con = openCon();
    $news_source_id = $row['id'];
    $user_id = $row['user_id'];
    $category_id = $row['category_id'];
    $title = str_replace("''","" ,$new->title);
    $short_description = str_replace("''","" , $new->description);
    $perman_link = $new->link;
    $date = $new->pubDate;
    $sql = "insert into news (title,short_description,permanlink,date,news_source_id,user_id,category_id)
    values('$title','$short_description','$perman_link','$date','$news_source_id','$user_id','$category_id')";
    mysqli_query($con,$sql) or die($con->error);
    closeCon($con);

}

/**
 * Este metodo obtiene todas las fuentes de la base de datos
 */
function getAll(){
    $con = openCon();
    $user = $_SESSION['user'];
    $id = $user['user_id'];
    $sql = "select n.id as source_id,n.name as source_name,url,c.name as category_name,user_id from newssources as n join categories as c on n.category_id = c.id where user_id= '$id' and n.disable = true";
    return $result = mysqli_query($con,$sql);
    closeCon($con);
}

/**
 * Este metodo obtiene las fuentes de la base de datos que estan habilitadas
 */
function getFeed(){
    $con = openCon();
    $sql = "select id,url,name,category_id,user_id from newssources where disable = true";
    return $result = mysqli_query($con,$sql);
    closeCon($con);
}

/**
 * Este metodo desabilita una fuente de noticias de la base de datos
 */
function deleteSource($data){
    $con = openCon();
    $id = mysqli_real_escape_string($con,$_GET['delete']);
    $sql = "update newssources set disable = false where id = '$id'";
    mysqli_query($con,$sql) or die($con->error);
    deleteNewsFromUser($id);
    header('location: ../index.php?register');
    closeCon($con);
}

/**
 * Este metodo elimina las noticias de una fuente 
 */

 function deleteNewsFromUser($id){
    $con = openCon();
    $sql = "delete from news where news_source_id = '$id'";
    mysqli_query($con,$sql) or die($con->error);
    closeCon($con);
 }


/**
 * Este metodo selecciona los atributos de una fuente de noticias de la base de datos
 */
function editSource($data){
    $con = openCon();
    $id = mysqli_real_escape_string($con,$data['edit']);
    $sql = "select id,name,url,category_id from newssources where id = '$id'";
    $result = mysqli_query($con,$sql) or die($con->error);
    if($result !=null){
        $row = $result->fetch_array();
        $GLOBALS['sourceId'] = $row['id'];
        $GLOBALS['name'] = $row['name'];
        $GLOBALS['url'] = $row['url'];
        $GLOBALS['category'] = $row['category_id'];
    }
    
    closeCon($con);
}

/**
 * Este metodo actualiza una fuente de noticas de la base de datos
 */
function updateSource($data){
    $con = openCon();
    $id = mysqli_real_escape_string($con,$data['id']);
    $name = mysqli_real_escape_string($con,$data['name']);
    $url = mysqli_real_escape_string($con,$data['url']);
    $category = mysqli_real_escape_string($con,$data['select']);
    $sql = "update newssources set name = '$name',url='$url',category_id='$category' where id = '$id' ";
    if(mysqli_query($con,$sql)){

    }
    header('location:../index.php?register');
    
}

/**
 * Este metodo selecciona las fuentes de noticias de cierto usuario en especifico
 */
function checkSource(){
    $idUser = 0;
    if($_SESSION && $_SESSION['user']){
        $user = $_SESSION['user'];
        $idUser = $user['user_id'];
    }
    $con = openCon();
    $sql = "select id,url,name,category_id,user_id from newssources where user_id = '$idUser'";
    $result = mysqli_query($con,$sql);
    if($result->num_rows > 0){
        return true;
    }else{
        closeCon($con);
        return false;
    }

}