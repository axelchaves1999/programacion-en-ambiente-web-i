<?php
require_once('connection.php');


/**
 * Este metodo inserta una notica en la tabla news de la base de datos
 */
function insertNew($new,$row){
    $con = openCon();
    $news_source_id = $row['id'];
    $user_id = $row['user_id'];
    $category_id = $row['category_id'];
    $title = str_replace("''","" ,$new->title);
    $short_description = str_replace("''","" , $new->description);
    $perman_link = $new->link;
    $date = $new->pubDate;
    $sql = "insert into news (title,short_description,permanlink,date,news_source_id,user_id,category_id)
    values('$title','$short_description','$perman_link','$date','$news_source_id','$user_id','$category_id')";
    mysqli_query($con,$sql) or die($con->error);
    closeCon($con);

}



/**
 * Este metodo obtiene las noticias de la tabla news de la base de datos
 */
function getAllNews($filter){
    $con = openCon();
    $user = $_SESSION['user'];
    $id = $user['user_id'];
    if($filter == 0 ){
        $sql = "select title,short_description,permanlink,category_id from news where user_id = '$id' order by news.id desc";
    }else{
        $sql = "select title,short_description,permanlink,category_id from news join categories on news.category_id = categories.id where categories.id = $filter and user_id = '$id' order by news.id desc";
    }
    return $result = mysqli_query($con,$sql);
    closeCon($con);
}

/**
 * Este metodo elimina las noticias de la base de datos
 */
function deleteNews(){
    $con = openCon();
    $sql = "delete from news";
    mysqli_query($con,$sql);
    closeCon($con);
}