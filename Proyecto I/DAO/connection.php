<?php

/*
    Esta funcion abre una conexion con la base de datos y la devuelve
*/
function openCon(){
    $dbHots = 'localhost';
    $dbUser = 'root';
    $dbPass = '';
    $db = 'my_news_cover';

    $con = new mysqli($dbHots,$dbUser,$dbPass,$db) or die ('Connect failed');
    return $con;
}

/*
    Esta funcion cierra una conexion que sea pasada por parametro
*/
function closeCon($con){
    $con->close();
}

?>
