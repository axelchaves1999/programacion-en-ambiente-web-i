<?php


//Revisa si existe un usuario con una session abierta, si existe le muestra la pantalla correspondiente
session_start();
$user = (isset( $_SESSION['user'])) ? $_SESSION['user']: null ;
if($user !== null){

    $type = $user['role_name'];
    if($type == 'user'){
        include('Includes/userDashboard.php');
    }else{
        include('Includes/admiDashboard.php');
    }
}else{
    session_destroy();
    //Si no existe un usuario con session abierta pregunta si va a registro o muestra el login
    if(isset($_GET['register'])){
        include('Includes/register.php');
    }else{
        include('Includes/login.php') ;
    }
}



