<?php
include('../DAO/sourceDAO.php');
include('../DAO/newsDAO.php');

accessFeed();

/**
 * Este metodo accede a un url que en este caso el url de un xml, extrae los items del url en este caso
 * serian las noticias y llama al metodo insertNew que agrega la notica a la base de datos
 */
function accessFeed(){
    deleteNews();
    $sources = getFeed();

    while($row = mysqli_fetch_array($sources)){

        $ch = curl_init($row['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36');
        
        
        $feed = curl_exec($ch);
        $rss = new SimpleXMLElement($feed);
        foreach($rss->channel->item as $item) {
            insertNew($item,$row);
        }
    }
}