<link rel="stylesheet" href="CSS/register.css">


<div class="main-container">
  <div class="wrapper">
    <div class="container ">
      <form action="DAO/userDAO.php" method="post">
        <div class="row">
          <div class="form-group col-md-6 col-sm-12">
            <label for="inputEmail4">First name</label>
            <input type="text" class="form-control" name="firstName" placeholder="First name">
          </div>
          <div class="form-group col-md-6 col-sm-12"">
        <label for=" inputPassword4">Last name</label>
            <input type="text" class="form-control" name="lastName" placeholder="Last name">
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-6 col-sm-12">
            <label for="inputEmail4">Email Address</label>
            <input type="email" class="form-control" name="email" placeholder="Email">
          </div>
          <div class="form-group col-md-6 col-sm-12"">
        <label for=" inputPassword4">Password</label>
            <input type="password" class="form-control" name="password" placeholder="Password">
          </div>
        </div>
        <div class="form-group">
          <label for="inputAddress">Address</label>
          <input type="text" class="form-control" name="address" placeholder="Address">
        </div>
        <div class="form-group">
          <label for="inputAddress2">Address 2</label>
          <input type="text" class="form-control" name="address2" placeholder="Address 2">
        </div>
        <div class="row">
          <div class="form-group col-md-6 col-sm-12">
            <label for="inputEmail4">Country</label>
            <select class="form-control" name="country">
              <option value="" selected disabled hidden>Choose here</option>
              <option value="1">Costa Rica</option>
              <option value="2">USA</option>
              <option value="3">Brazil</option>
              <option value="4">Mexico</option>
              <option value="5">Canada</option>
            </select>
          </div>
          <div class="form-group col-md-6 col-sm-12"">
        <label for=" inputPassword4">City</label>
            <input type="text" class="form-control" name="city" placeholder="City">
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6 col-sm-12">
            <label for="inputEmail4">Zip/Postal Code</label>
            <input type="text" class="form-control" name="zip" placeholder="Zip/Postal Code">
          </div>
          <div class="form-group col-md-6 col-sm-12"">
        <label for=" inputPassword4">Phone Number</label>
            <input type="text" class="form-control" name="phone" placeholder="Phone Number">
          </div>
        </div>
        <div class="row">
          <button name="add" type="submit" class="btn btn-primary mt-3">Sign up</button>
          <a class="btn btn-secondary mt-2" href="index.php">Cancel</a>
        </div>
      </form>
    </div>
  </div>
</div>