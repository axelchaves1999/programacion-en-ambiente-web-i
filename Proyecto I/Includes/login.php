<link rel="stylesheet" href="CSS/login.css">

<div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
    <div class="card card border-0">
        <div class="row d-flex">
            <div class="col-lg-6">
                <div class="card1 mt-5">
                    <div class="row px-3 justify-content-center mt-4 mb-5 border-line"> <img src="IMG/logo.jpg" class="image"> </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card2 card border-0 px-4 py-5">
                    <form action="DAO/userDAO.php" method="post">
                        <div class="row px-3"> <label class="mb-1">
                                <h6 class="mb-0 text-sm">Email Address</h6>
                            </label> <input class="mb-4" type="text" name="email" placeholder="Enter a valid email address"> </div>
                        <div class="row px-3"> <label class="mb-1">
                                <h6 class="mb-0 text-sm">Password</h6>
                            </label> <input type="password" name="password" placeholder="Enter password"> </div>

                        <div class="row mt-5 px-3"> <button name="login" type="submit" class="btn btn-blue text-center">Login</button> </div>
                        <div class="row mb-4 pt-4"> <small class="font-weight-bold">Don't have an account? <a class="text-danger " href="index.php?register=true">Register</a></small> </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="bg-blue py-4">
        </div>
    </div>
</div>