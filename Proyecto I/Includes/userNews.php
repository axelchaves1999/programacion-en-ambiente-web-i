<?php
require_once('./DAO/categoryDAO.php');
require_once('./DAO/newsDAO.php');
require_once('./DAO/sourceDAO.php');
?>

<?php
$sources = checkSource();
if(!$sources){
    header('location: index.php?register');
}
?>



<div class="container mt-5">
    <div class="jumbotron jumbotron-fluid">
        <div class="container text-center">
            <h1 class="display-5">Your unique News Cover</h1>
            <hr>
        </div>
    </div>
</div>

<div class="container ">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="index.php">All news</a>
        </li>
        <?php
        $result = select();
        while ($row = mysqli_fetch_array($result)) : ?>
            <li class="nav-item">
                <a class="nav-link" href="index.php?s=<?php echo $row['id'] ?>"><?php echo $row['name'] ?></a>
            </li>

        <?php endwhile; ?>
    </ul>
</div>

<div class="container mt-3">
    <div class="row justify-content-center ">

    <?php 
        $idCateogry = (isset($_GET['s']))?$_GET['s']:0;
        $resultNews = getAllNews($idCateogry);
        while($row = mysqli_fetch_array($resultNews)) :?>
          
  
        <div class="card m-3" style="width: 15rem;">
            <div class="card-body">
                <p class="card-text"><strong><?php echo $row['title'] ?></strong></p>
                <p class="card-text"><?php echo $row['short_description'] ?></p>
                <a href="<?php echo $row['permanlink'] ?> target="_blank" class="card-link"><?php echo $row['permanlink'] ?></a>
            </div>
        </div>
    <?php endwhile; ?>

 
    </div>
</div>