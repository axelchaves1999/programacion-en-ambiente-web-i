<link rel="stylesheet" href="CSS/admiDashboard.css">


<?php include('Includes/navBar.php'); ?>

<?php 
    include('DAO/categoryDAO.php');
    $result = select();

?>

<div class="container">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Categories</h1>
            <hr>
        </div>
    </div>
</div>

<div class="container">
    <form action="DAO/categoryDAO.php" method="post">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col"><input class="form-control" type="text" name="name" placeholder="Name" value="<?php echo $name ?>"></th>
                    <th scope="col">
                    
                        <?php if(isset($_GET['edit'])):?>
                            <button name="update" type="submit" class="btn btn-primary" >Edit</button>

                        <?php else :?>
                            <button name="add" type="submit" class="btn btn-primary">Add new</button>

                        <?php endif; ?>
                    </th>
                </tr>
            </thead>
    </form>
    <tbody>
        <?php
            $cont = 0;
            while($row = mysqli_fetch_array($result)) : $cont++ ?>
            <tr>
                <th><?php echo $cont.'.' ?></th>
                <th class="text-center"><?php echo $row['name'] ?></th>
                <th>
                <a class="btn btn-success mt-2" href="index.php?edit=<?php echo$row['id'] ?>">Edit</a>
                <a class="btn btn-danger mt-2" href="DAO/categoryDAO.php?delete=<?php echo$row['id'] ?>">Delete</a>
                </th>
                <th> <input type="hidden" name="id" value="<?php echo $id; ?>"></th>
            </tr>

        <?php endwhile; ?>
    </tbody>
    </table>
</div>