<?php
include('DAO/categoryDAO.php');
$resultCategory = select();
?>

<?php
include('DAO/sourceDAO.php');
$resultSource = getAll();
?>



<div class="container mt-3">
    <div class="jumbotron jumbotron-fluid">
        <div class="container text-center">
            <h1 class="display-4">New sources</h1>
            <hr>
        </div>
    </div>
</div>

<div class="container">
    <form action="DAO/sourceDAO.php" method="post" >
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th>#</th>
                    <th><input class="form-control" type="text" name="name" placeholder="Name" value="<?php echo $name ?>"></th>
                    <th><input class="form-control" type="text" name="url" placeholder="RSS URL" value="<?php echo $url ?>"></th>
                    <th>
                        <select name="select" class="form-select" aria-label="Default select example">
                            <option value="" disabled selected>Select a category</option>
                            <?php
                        
                            while ($row = mysqli_fetch_array($resultCategory)) : ?>
                                    <?php if((isset($_GET['edit'])) and $row['id'] == $category): ?>
                                        <option value="<?php echo $row['id']  ?>" selected><?php echo $row['name'] ?></option>
                                    <?php else : ?>
                                        <option value="<?php echo $row['id']  ?>"><?php echo $row['name'] ?></option>
                                    <?php endif; ?>

                            <?php endwhile ?>

                        </select>
                    </th>
                    <th scope="col">
                    <?php
                    if (isset($_GET['edit'])) : ?>
                        <button name="update" type="submit" class="btn btn-primary">Edit new</button> 
                    <?php else : ?>
                        <button name="add" type="submit" class="btn btn-primary">Add new</button> 

                    <?php endif; ?>
                    </th>

                    <th>
                    <input type="hidden" name="id" value="<?php echo $sourceId; ?>">
                    </th>
                </tr>

            </thead>
    </form>
    <tbody>
        
        <?php
 
            $contador = 0;
            while ($row = mysqli_fetch_array($resultSource)) :  $contador++; ?>
            <tr>
                <td><?php echo $contador . '.' ?></td>
                <td class="text-center"><?php echo $row['source_name'] ?></td>
                <td class="text-center"><?php echo $row['url'] ?></td>
                <td class="text-center"><?php echo $row['category_name'] ?></td>
                <td>
                    <a class="btn btn-success mt-2" href="index.php?edit=<?php echo $row['source_id'] ?>">Edit</a>
                    <a class="btn btn-danger mt-2" href="DAO/sourceDAO.php?delete=<?php echo $row['source_id'] ?>">Delete</a>
                </td>
            </tr>


        <?php endwhile; ?>
    </tbody>
    </table>
</div>