<?php

readContries();
readTemperature();

function readContries()
{

    $myFile = "countries.txt";
    $fh = fopen($myFile, "r");
    if ($fh) {
        while (!feof($fh)) {
            $line = fgets($fh);
            $country = explode(',', $line);
    
            for ($i = 0; $i < count($country); $i++) {
                $linea = explode('=>', $country[$i]);


                for ($y = 0; $y < count($linea); $y++) {
                    if ($y % 2 == 0) {
                        $countries[] = $linea[$y];
                    } else {
                        $capitals[] = $linea[$y];
                    }
                }
            }

            for ($i = 0; $i < count($countries); $i++) {
                echo nl2br("The capitial of $countries[$i] is $capitals[$i]" . "\r");
            }
        }
        fclose($fh);
    }
}



function readTemperature(){
    $myfile = "temperature.txt";
    $fh = fopen($myfile,'r');
    if($fh){
        while(!feof($fh)){
            $line = fgets($fh);
            $temperature = explode(',',$line);

            for ($i=0; $i < count($temperature) ; $i++) { 
                $temperatures[] = (int)($temperature[$i]);
            }
        }
        $num = 0;
        for ($i=0; $i <count($temperatures) ; $i++) { 
            $num += $temperatures[$i];
        }
        $num = $num / count($temperatures); 
        echo nl2br("\nAverage Temperature is: $num \n");

        $unique = array_unique($temperatures);
        sort($unique);
        echo nl2br("List of 5 lowest temperatures (no duplicates) : $unique[0],$unique[1],$unique[2],$unique[3],$unique[4]\n");
        rsort($unique);
        echo nl2br("List of 5 highest temperatures (no duplicate) : $unique[0],$unique[1],$unique[2],$unique[3],$unique[4]");
    }
    fclose($fh);

}