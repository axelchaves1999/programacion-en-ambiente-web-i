
<?php

class User_model extends CI_Model {

    public function authenticate($email,$password){
        $this->db->where('email',$email);
        $this->db->where('password',$password);
        $query = $this->db->get('workshop_viii');
        return $query->result();
    }

    public function select(){
        return $this->db->get('workshop_viii');
    }

    public function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('workshop_viii');
    }

    public function selectId($id){
        $this->db->where('id',$id);
        $query = $this->db->get('workshop_viii');
        return $query->result();
    }

    public function update($email,$phone,$id){
        $this->db->set('email',$email);
        $this->db->set('phone',$phone);
        $this->db->where('id',$id);
        $this->db->update('workshop_viii');
    }

}