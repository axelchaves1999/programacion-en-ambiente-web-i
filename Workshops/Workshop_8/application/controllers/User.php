<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
          
    }

	public function login(){
        $this->load->view('login');
    }

    public function dashboard(){
        $data['users'] = $this->User_model->select();
        $this->load->view('dashboard',$data);
    }

    public function edit(){
        $id = $this->input->get('id');
        $data['user'] = $this->User_model->selectId($id);
        $this->load->view('editUser',$data);
    }

    public function authenticate(){
       
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $user = $this->User_model->authenticate($email,$password);
        
        if($user){
            header('location: dashboard');
        }else{
            header('location: login');
        }
       
    }

    public function delete(){
        $id = $this->input->get('id');
        $this->User_model->delete($id);
        header('location:dashboard');
    }

    public function update(){
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $id = $this->input->post('id');
        $this->User_model->update($email,$phone,$id);
        header('location: dashboard');
    }


}