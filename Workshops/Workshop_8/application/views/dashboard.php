<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Login</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>

<div class="container">
<table class="table table-striped mt-5">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Apellido</th>
      <th scope="col">Correo</th>
      <th scope="col">Telefono</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
            <?php 
                $count = 0;
                foreach($users->result() as $user): $count++?>
                <tr>
                    <td><?php echo $count ?></td>
                    <td><?php echo $user->first_name ?></td>
                    <td><?php echo $user->last_name ?></td>
                    <td><?php echo $user->email ?></td>
                    <td><?php echo $user->phone ?></td>
                    <td>
                    <a class="btn btn-success" href="edit?id=<?php echo $user->id ?>" role="button">Editar</a>
                    <a class="btn btn-danger" href="delete?id=<?php echo $user->id?>" role="button">Delete</a>
                    </td>
                   

                </tr>
            <?php endforeach ?>
            
      
  </tbody>
</table>

</div>

</body>
</html>





