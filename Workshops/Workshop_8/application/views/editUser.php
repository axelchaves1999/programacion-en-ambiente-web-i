<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit user</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <div class="container mt-4">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-5">Name:</h1>
                <h1 class="display-5">Last Name:</h1>
            </div>
        </div>

    </div>

    <div class="container ">
        <?php 
        foreach($user as $row) :?>
        <form method="post" action="<?php echo base_url() ?>index.php/user/update" >

            <div class="form-group">
                <label for="exampleInputEmail1">Correo</label>
                <input type="text" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Correo" value="<?php echo $row->email ?>">
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Telefono</label>
                <input type="text" class="form-control" name="phone" aria-describedby="emailHelp" placeholder="Telefono" value="<?php echo 
                $row->phone ?>">
            </div>

            <div class="row justify-content-center">
            <div class=" form-group col-md-6">        
                <input type="hidden" name="id" value="<?php echo $row->id; ?>">
            </div>
        </div>

        <button class="btn btn-primary " type="submit" name="update">Actualizar</button>

        </form>
        <?php endforeach ?>
    </div>

</head>

<body>

</body>

</html>