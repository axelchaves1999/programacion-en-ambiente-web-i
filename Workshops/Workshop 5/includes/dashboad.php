
<?php 
include('./includes/navbar.php');
?>

<div class="container mt-5">
    <form method="post" action="database/insert.php">

        <div class="row justify-content-center">
            <div class=" form-group col-md-6">        
                <input type="hidden" name="id" value="">
            </div>
        </div>

        <div class="row justify-content-center">
            <div class=" form-group col-md-6">
                <label for="exampleInputEmail1">Cédula</label>
                <input class="form-control" type="text" name="Cedula" value="">
            </div>
        </div>

        <div class="row justify-content-center mt-2">
            <div class="form-group  col-md-6">
                <label for="exampleInputPassword1">Nombre</label>
                <input class="form-control" type="text" name="Nombre" value="">
            </div>
        </div>

        <div class="row justify-content-center mt-2">
            <div class="form-group  col-md-6">
                <label for="exampleInputPassword1">Primer apellido</label>
                <input class="form-control" type="text" name="Apellido" value="">
            </div>
        </div>


        <div class="row justify-content-center mt-2">
            <div class="form-group  col-md-6 ">
                <label for="exampleInputPassword1">Correo electronico</label>
                <input class="form-control" type="text" name="Correo" value="">
            </div>
        </div>

        <div class="row justify-content-center mt-2 ">
            <div class="form-group col-md-6 ">
                <label for="exampleInputPassword1">Carrera</label>
                <select name="Opcion" class="form-select" aria-label="Default select example">
     
 
                   
                </select>

            </div>
        </div>

        <div class="row justify-content-center mt-3 ">
            <div class="form-group col-md-6 ">
        
                    <button class="btn btn-primary " type="submit" name="add">Agregar</button>
                
            </div>
        </div>


    </form>
</div>