<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crud usuarios</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>
    <?php require_once 'process.php'?>

    <?php 
        if (isset($_SESSION['message'])): ?>
    <div class="alert alert-<?=$_SESSION['msg_type']?>">
        <?php 
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        ?>
    </div>
    <?php endif ?>


    <?php 
        $mysqli = new mysqli('localhost','root','','programacion_web_I') or die(mysqli_error($mysqli));
        $result = $mysqli->query("SELECT ID,NOMBRE,DIRECCION, PAIS FROM WORKSHOP_I ") or die($mysqli->error);
    ?>


    <div id="contenedor" class="container">

    </div>
    <div class="container pt-5">
        <form method="post" action="process.php">
            <table class="table">
                <thead>
                    <tr>
                        <input type="hidden" name="id" value="<?php echo $id ?>">
                        <th>#</th>
                        <th><input class="form-control" type="text" name="name" placeholder="Nombre"
                                value="<?php echo $name ?>"></th>
                        <th><input class="form-control" type="text" name="direction" placeholder="Dirección"
                                value="<?php echo $direction ?>"></th>
                        <th><input class="form-control" type="text" name="country" placeholder="País"
                                value="<?php echo $country ?>"></th>
                        <?php 
                            if($update):
                                 
                        ?>
                        <th><button id="edit" class="btn btn-primary" type="submit" name="update">Editar</button></th>

                        <?php else: ?>

                        <th><button id="add" class="btn btn-primary" type="submit" name="save">Agregar</button></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>

                    <?php 
                        $contador = 0;
                        while ($row = $result->fetch_assoc()):  $contador++;?>
                    <tr>
                        <td class="table=secondary"><?php echo $contador; ?></td>
                        <td class="table=secondary"><?php echo $row['NOMBRE']; ?></td>
                        <td class="table=secondary"><?php echo $row['DIRECCION']; ?></td>
                        <td class="table=secondary"><?php echo $row['PAIS']; ?></td>
                        <td>
                            <a href="index.php?edit=<?php echo $row['ID']; ?>" class="btn btn-info">Editar</a>
                            <a href="process.php?delete=<?php echo $row['ID']; ?>" class="btn btn-danger">Borrar</a>
                        </td>
                    </tr>

                    <?php endwhile; ?>




                </tbody>
            </table>
        </form>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="js/app.js"></script>
</body>

</html>