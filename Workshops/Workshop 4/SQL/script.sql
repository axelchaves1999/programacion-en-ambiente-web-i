

/*carreras */
CREATE TABLE workshop_IV_I(
    id INT PRIMARY KEY AUTO_INCREMENT,
    codigo VARCHAR(20) NOT  NULL,
    nombre VARCHAR(30) NOT NULL
)

/*registro*/
CREATE TABLE workshop_IV_II(
    id INT PRIMARY KEY AUTO_INCREMENT,
    cedula VARCHAR(30) NOT NULL,
    nombre VARCHAR (30) NOT NULL,
    correo VARCHAR(30) NOT NULL,
    carrera INT NOT NULL,
    fecha TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT FOREIGN KEY fk_II_I (carrera) REFERENCES workshop_IV_I(id)
)

ALTER TABLE workshop_iv_ii add apellido varchar(30)


