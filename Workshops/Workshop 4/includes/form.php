<?php
require_once './database/connection.php';
$con = OpenCon();
$sql = 'select id,codigo,nombre from workshop_IV_I';
$result = mysqli_query($con, $sql);
?>

<?php
  
require_once './database/edit.php';
        
?>




<div class="container mt-5">
    <form method="post" action="database/insert.php">

        <div class="row justify-content-center">
            <div class=" form-group col-md-6">        
                <input type="hidden" name="id" value="<?php echo $id; ?>">
            </div>
        </div>

        <div class="row justify-content-center">
            <div class=" form-group col-md-6">
                <label for="exampleInputEmail1">Cédula</label>
                <input class="form-control" type="text" name="Cedula" value="<?php echo $document ?>">
            </div>
        </div>

        <div class="row justify-content-center mt-2">
            <div class="form-group  col-md-6">
                <label for="exampleInputPassword1">Nombre</label>
                <input class="form-control" type="text" name="Nombre" value="<?php echo $name ?>">
            </div>
        </div>

        <div class="row justify-content-center mt-2">
            <div class="form-group  col-md-6">
                <label for="exampleInputPassword1">Primer apellido</label>
                <input class="form-control" type="text" name="Apellido" value="<?php echo $surname ?>">
            </div>
        </div>


        <div class="row justify-content-center mt-2">
            <div class="form-group  col-md-6 ">
                <label for="exampleInputPassword1">Correo electronico</label>
                <input class="form-control" type="text" name="Correo" value="<?php echo $email ?>">
            </div>
        </div>

        <div class="row justify-content-center mt-2 ">
            <div class="form-group col-md-6 ">
                <label for="exampleInputPassword1">Carrera</label>
                <select name="Opcion" class="form-select" aria-label="Default select example">
     
 
                        <?php
                            while ($row = mysqli_fetch_array($result)) :?>

                            <option value="<?php echo $row['id'] ?>"><?php echo $row['nombre'] ?></option>
    
    
                        <?php endwhile ?>
     
                </select>

            </div>
        </div>

        <div class="row justify-content-center mt-3 ">
            <div class="form-group col-md-6 ">
                <?php
                    if(isset($_GET['edit'])):?>
                    <button class="btn btn-primary " type="submit" name="update">Editar</button>
                <?php else :?>
                    <button class="btn btn-primary " type="submit" name="add">Agregar</button>
                <?php endif?>
            </div>
        </div>


    </form>
</div>