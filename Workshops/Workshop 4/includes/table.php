
<?php 
    include_once './database/select.php';
?>

<div class="container">
<table class="table table-striped mt-5">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Apellido</th>
      <th scope="col">Correo</th>
      <th scope="col">Carrera</th>
    </tr>
  </thead>
  <tbody>
    
    <?php 
        $contador = 0;
        while($row = mysqli_fetch_array($result)): $contador++ ?>
       
        <tr>
            <td><?php echo $contador.'.' ?></td>
            <td><?php echo $row['nombre'] ?></td>
            <td><?php echo $row['apellido'] ?></td>
            <td><?php echo $row['correo']?></td>
            <td><?php echo  $row['carrera_nombre']?></td>
            <td><a class="btn btn-success" href="./index.php?edit=<?php echo $row['matricula_id'] ?>" role="button">Editar</a></td>
            <td><a class="btn btn-danger" href="./database/delete.php?delete=<?php echo $row['matricula_id'] ?>" role="button">Eliminar</a></td>
        </tr>
    
        <?php endwhile ?>
  </tbody>
</table>

</div>


