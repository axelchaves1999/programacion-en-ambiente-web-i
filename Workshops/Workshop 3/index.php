<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Categoria de libros</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>
    <!--Incluye el archivo control-->
    <?php require_once 'control.php' ?>


  <!--Si existe una session abierta revisa si existe une mensage y lo muestra-->
    <?php 
        if(isset($_SESSION['message'])): ?>

    <div class="alert alert-<?=$_SESSION['msg_type']?>" role="alert">
       <?php
            echo $_SESSION['message'];
            unset($_SESSION['message']);
       ?>
    </div>

    <?php endif ?>


    <?php 
        require_once 'connection.php';
        $con = OpenCon();
        $sql = "SELECT id,nombre,tipo,genero FROM workshop_II";
        $result = mysqli_query($con,$sql);
    ?>




    <div class="container mt-5">
        <form method="post" action="control.php">
            <table class="table table-light">
                <table class="table table-light">
                    <thead class="thead-light">
                        <tr>
                            <th><input type="hidden" name="id" value="<?php echo $id; ?>"></th>
                            <th>#</th>
                            <th><input class="form-control" type="text" name="name" placeholder="Nombre" value="<?php echo $name?>"> </th>
                            <th><input class="form-control" type="text" name="type" placeholder="Tipo" value="<?php echo $type ?>"></th>
                            <th><input class="form-control" type="text" name="genre" placeholder="Genero" value="<?php echo $genre ?>"></th>

                            <?php 
                                if($update == true):?>
                                <th><button class="btn btn-primary" type="submit" name="update">Editar</button></th>
                            <?php else: ?>
                                <th><button class="btn btn-primary" type="submit" name="add">Agregar</button></th>
                            <?php endif ?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        $contador = 0;
                        while($row = mysqli_fetch_array($result)): $contador++ ?>
                            <tr>
                                <td><?php echo $contador.'.'?></td>
                                <td><?php echo $row['nombre']?></td>
                                <td><?php echo $row['tipo']?></td>
                                <td><?php echo $row['genero']?></td>
                                <td>
                                <a class="btn btn-success" href="index.php?edit=<?php echo $row['id'] ?>" role="button">Editar</a>
                                <a class="btn btn-danger" href="control.php?delete=<?php echo $row['id']?>" role="button">Eliminar</a>
                                </td>                  
                            </tr>

                        <?php endwhile; CloseCon($con);?>

                    </tbody>
                </table>
            </table>
        </form>
    </div>
</body>

</html>