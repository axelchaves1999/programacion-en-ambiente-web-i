<?php
require_once('DbConnection.php');
require_once('Student.php');
require_once('StudentDao.php');

$limit = $argv[1];
$studetDAO = new StudentDao();
$students = $studetDAO->getAll($limit);

while($row = mysqli_fetch_array($students)){
    $newStudent = new Student($row['full_name'],$row['document'],$row['age'],$row['id']);
    echo $newStudent->toCsv().PHP_EOL;
}