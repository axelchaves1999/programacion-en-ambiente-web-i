<?php

class Student{

    public $id;
    public $fullName;
    public $document;
    public $age;

    function __construct($fullName,$document,$age, $id=0)
    {   
        $this->fullName = $fullName;
        $this->document = $document;
        $this->age = $age;
        $this->id = $id;
    }

    function toCsv(){
        return "{$this->fullName},{$this->document},{$this->age},{$this->id}";
    }

}